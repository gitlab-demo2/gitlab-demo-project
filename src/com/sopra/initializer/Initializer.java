package com.sopra.initializer;

import java.util.Arrays;
import java.util.List;

import com.sopra.models.Dish;
import com.sopra.models.Trader;
import com.sopra.models.Transaction;

public class Initializer {
	
	List<Dish> menu;
		
	public List<Dish> getMenu()
	{
		menu = Arrays.asList(
				
			    new Dish("Pork", false, 800, Dish.Type.MEAT),
			    new Dish("Burger", true, 700, Dish.Type.OTHER),
			    new Dish("Chicken", false, 400, Dish.Type.MEAT),
			    new Dish("French fries", true, 530, Dish.Type.OTHER),
			    new Dish("Rice", true, 350, Dish.Type.OTHER),
			    new Dish("Season fruit", true, 120, Dish.Type.OTHER),
			    new Dish("Pizza", true, 550, Dish.Type.OTHER),
			    new Dish("Prawns", false, 300, Dish.Type.FISH),
			    new Dish("Salmon", false, 450, Dish.Type.FISH) );
		
		return menu;
		
	}
	
	public List<Transaction> getTransactions()
	{
		Trader raoul = new Trader("Raoul", "Cambridge");
		Trader mario = new Trader("Mario","Milan");
		Trader alan = new Trader("Alan","Cambridge");
		Trader brian = new Trader("Brian","Cambridge");

		List<Transaction> transactions = Arrays.asList(
		    new Transaction(brian, 2011, 300,"USD"),
		    new Transaction(raoul, 2012, 1000,"USD"),
		    new Transaction(raoul, 2011, 400,"YEN"),
		    new Transaction(mario, 2012, 710,"YEN"),
		    new Transaction(mario, 2012, 700,"RUPEE"),
		    new Transaction(alan, 2012, 950,"RUPEE")
		);
		
		return transactions;
	}
	

}
