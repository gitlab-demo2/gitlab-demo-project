package com.sopra.streamoperations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.sopra.initializer.Initializer;
import com.sopra.models.Dish;

public class FindAndMatch {

	 static List<Dish> menu;
	 static boolean isHealthy;
		
		public static void main(String[] args) {
			
			menu=new Initializer().getMenu();
			
			//AnyMatch
			if(menu.stream().anyMatch(Dish::isVegetarian)){
			   System.out.println("The menu is (somewhat) vegetarian friendly!!");
			}
			
			//AllMatch

			isHealthy = menu.stream()
                    .allMatch(d -> d.getCalories() < 1000);
			
			//NoneMatch
			isHealthy = menu.stream()
                    .noneMatch(d -> d.getCalories() >= 1000);
			
			//FindAny
			Optional<Dish> dish = menu.stream()
								 .filter(Dish::isVegetarian)
								 .findAny();
			
			dish.ifPresent(System.out::print);
			
			//FIndFirst
			List<Integer> someNumbers = Arrays.asList(1, 2, 3, 4, 5);
		    someNumbers.stream()
			           .map(x -> x * x)
			           .filter(x -> x % 3 == 0)
			           .findFirst().ifPresent(System.out::print);
			             
		}	

}
