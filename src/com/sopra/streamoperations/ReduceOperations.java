package com.sopra.streamoperations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.sopra.initializer.Initializer;
import com.sopra.models.Dish;

public class ReduceOperations {

	static List<Dish> menu;
	 static boolean isHealthy;
		
		public static void main(String[] args) {
			
			menu=new Initializer().getMenu();
			
			List<Integer> numbers=Arrays.asList(1,2,3,4,5);
			
			int sum =numbers.stream().reduce(0, (a, b) -> a + b);
			System.out.println("Sum "+sum);
			
			int product =numbers.stream().reduce(1, (a, b) -> a * b);
			System.out.println("Product "+product);
			
			sum = numbers.stream().reduce(0, Integer::sum);
			System.out.println("sum "+sum);

			
			Optional<Integer> sum1=numbers.stream().reduce((a, b) -> (a + b));
			System.out.println("sum1 "+sum1);

			
			Optional<Integer> max = numbers.stream().reduce(Integer::max);
			System.out.println("max "+max);

			
			Optional<Integer> min = numbers.stream().reduce(Integer::min);
			System.out.println("min "+min);

				             
		}	
	
}
