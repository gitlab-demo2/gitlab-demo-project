package com.sopra.streamoperations;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.sopra.initializer.Initializer;
import com.sopra.models.Trader;
import com.sopra.models.Transaction;

public class QuizStreams {

	static List<Transaction> transactions;
	
	public static void main(String args[])
	{
		transactions=new Initializer().getTransactions();
		
		//Listing 5.1. Find all transactions in 2011 and sort by value (small to high)
		
		transactions.stream()
		            .filter(t->t.getYear()==2011)
		            .sorted(Comparator.comparing(Transaction::getValue))
		            .forEach(System.out::println);
		
		//Listing 5.2. What are all the unique cities where the traders work?
		
		transactions.stream().map(transactions->transactions.getTrader().getCity())
		.distinct().forEach(System.out::println);
		
	
	  //Listing 5.3. Find all traders from Cambridge and sort them by name
	   
	   transactions.stream().map(transaction -> transaction.getTrader())
	    		     		.filter(trader -> trader.getCity().equalsIgnoreCase("Cambridge")).distinct()
	    		     		.sorted(Comparator.comparing(Trader::getName))
                            .forEach(System.out::println);

	  //Listing 5.4. Return a string of all traders� names sorted alphabetically

	   String traders=transactions.stream().map(transaction -> transaction.getTrader().getName())
	                 .distinct().sorted()
	                 .reduce("Combined String",(n1,n2)->n1+" "+n2);
	 
	   System.out.println("Traders "+traders);
	   
	   String traderStr =transactions.stream()
					                  .map(transaction -> transaction.getTrader().getName())
					                  .distinct()
					                  .sorted()
					                  .collect(Collectors.joining());
	   
	   System.out.println("Traders "+traderStr);
	   
	   //Listing 5.5. Are any traders based in Milan?
	   
	   boolean milanTrader=transactions.stream().map(transaction -> transaction.getTrader())
	                         .anyMatch(t->t.getCity().equalsIgnoreCase("Milan"));
	   
	   
	   System.out.println("Are any traders based in Milan? "+milanTrader);
	   
	  //Listing 5.6. Print all transactions� values from the traders living in Cambridge

	   transactions.stream()
	               .filter(t->t.getTrader().getCity().equalsIgnoreCase("Cambridge"))
	               .forEach(t->System.out.println(t.getValue()));		 

	   //Listing 5.7. What�s the highest value of all the transactions?

	  Optional<Integer> highestValue= transactions.stream().map(t->t.getValue()).reduce(Integer::max);
	  System.out.println("highestValue "+highestValue.get());
	  
	  Optional<Transaction> highestTransaction = transactions.stream()
              .max(Comparator.comparing(Transaction::getValue));

	  System.out.println("highestTransaction "+highestTransaction.get().getValue());
	  
	  //Listing 5.8. Find the transaction with the smallest value
	  
	  Optional<Integer> smallestValue= transactions.stream().map(t->t.getValue()).reduce(Integer::min);
	  System.out.println("smallestValue "+smallestValue.get());

	  Optional<Transaction> smallestTransaction = transactions.stream()
			                                      .min(Comparator.comparing(Transaction::getValue));
    
	  System.out.println("smallestTransaction "+smallestTransaction.get().getValue());

	
		
	}
}
