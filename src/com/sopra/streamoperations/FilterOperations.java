package com.sopra.streamoperations;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.sopra.initializer.Initializer;
import com.sopra.models.Dish;

public class FilterOperations {

	static List<Dish> menu;

	
	public static void main(String[] args) {
		
		menu=new Initializer().getMenu();
		
		//Filtering a stream with a predicate
		
        List<Dish> vegMenu=menu.stream().filter(Dish::isVegetarian).collect(Collectors.toList());
        vegMenu.forEach(System.out::println);	
        
        //Filtering unique elements
        //Streams also support a method called distinct that returns a stream with unique elements (according to the implementation of the hashCode and equals methods of the objects produced by the stream).

        List<Integer> numbers = Arrays.asList(1, 2, 1, 3, 3, 2, 4);
        
        numbers.stream()
               .filter(i -> i % 2 == 0)
               .distinct()
               .forEach(System.out::println);
       
        // Truncating a stream
        
        List<Dish> dishes = menu.stream()
			               .filter(d -> d.getCalories() > 300)
			               .limit(3)
			               .collect(Collectors.toList());
       
        //dishes.forEach(System.out::println);
        
        //Skipping elements in a stream
        
        dishes = menu.stream()
                .filter(d -> d.getCalories() > 300)
                .skip(2)
                .collect(Collectors.toList());
        
        dishes.forEach(System.out::println);
 
	}

}

/*
In Java 8, Predicate is a functional interface, which accepts an argument and returns a boolean. Usually, it used to apply in a filter for a collection of objects.

@FunctionalInterface
public interface Predicate<T> {
  boolean test(T t);
}

*/