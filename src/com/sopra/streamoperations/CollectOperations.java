package com.sopra.streamoperations;



import static java.util.stream.Collectors.averagingInt;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.maxBy;
import static java.util.stream.Collectors.minBy;
import static java.util.stream.Collectors.summarizingInt;
import static java.util.stream.Collectors.summingInt;

import java.util.Comparator;
import java.util.HashSet;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.sopra.initializer.Initializer;
import com.sopra.models.Dish;
import com.sopra.models.Transaction;


public class CollectOperations {
	
    static List<Transaction> transactions;
	static List<Dish> menu;
    public enum CaloricLevel { DIET, NORMAL, FAT }
    
    //Comments Added

	//We assume that you�ve imported all the static factory methods of the Collectors class with ,so you can write counting() instead of Collectors.counting() and so on.

	
	public static void main(String[] args) {
		
		menu=new Initializer().getMenu();
		transactions=new Initializer().getTransactions();

		//Listing 6.1. Grouping transactions by currency
		        
		Map<String,List<Transaction>> currencyTrasaction=transactions.stream()
				                                         .collect(groupingBy(Transaction::getCurrency));
		currencyTrasaction.forEach((k,v)->System.out.println("Currency "+k+" List "+v));
        
        // Count the number of dishes in the menu, using the collector returned by the counting factory method:
       
        long howManyDishes = menu.stream().collect(counting());
        System.out.println("howManyDishes "+howManyDishes);
        
        howManyDishes = menu.stream().count();
        System.out.println("howManyDishes "+howManyDishes);
        
        //Finding Maximum and minimum
        
        Comparator<Dish> dishCaloriesComparator =
        Comparator.comparingInt(Dish::getCalories);

        Optional<Dish> mostCalorieDish =menu.stream()
        	        						.collect(maxBy(dishCaloriesComparator));
        
        System.out.println("mostCalorieDish "+mostCalorieDish);

        mostCalorieDish =menu.stream().collect(Collectors.reducing(
        	             (d1, d2)-> d1.getCalories() > d2.getCalories() ? d1 : d2));
        
        System.out.println("mostCalorieDish "+mostCalorieDish);
        
        
        Optional<Dish> leastCalorieDish =menu.stream().collect(minBy(dishCaloriesComparator));
        System.out.println("leastCalorieDish "+leastCalorieDish);

        
        //Summing Operation
        
        int totalCalories = menu.stream().collect(summingInt(Dish::getCalories));
        
        totalCalories = menu.stream().collect(Collectors.reducing(
                                     0, Dish::getCalories,(i, j) -> i + j));
        
        totalCalories =menu.stream().collect(Collectors.reducing(0,Dish::getCalories,Integer::sum));
        
        totalCalories =menu.stream().map(Dish::getCalories).reduce(Integer::sum).get();
        
        totalCalories =menu.stream().map(Dish::getCalories).reduce(Integer::sum).get();

        System.out.println("totalCalories "+totalCalories);

        
        //Averaging Operation
        
        double avgCalories =menu.stream().collect(averagingInt(Dish::getCalories));
        System.out.println("avgCalories "+avgCalories);

        //SummaryStatistics Operation
        
        IntSummaryStatistics menuStatistics =menu.stream().collect(summarizingInt(Dish::getCalories));
        System.out.println("menuStatistics "+menuStatistics);
        
        //Joining Operation
        
        String shortMenu = menu.stream().map(Dish::getName).collect(joining(" "));
        
        shortMenu = menu.stream().map(Dish::getName).collect(Collectors.reducing( (s1, s2) -> s1 + s2 ) ).get();
        
        shortMenu = menu.stream().collect(Collectors.reducing( "", Dish::getName, (s1, s2) -> s1 + s2 ) );
        
        System.out.println("shortMenu "+shortMenu);


        //Grouping Operations
        
        Map<Dish.Type,List<Dish>> dishesByType =menu.stream().collect(groupingBy(Dish::getType));  
        dishesByType.forEach((k,v)->System.out.println("Dish Type "+k+" Dishes "+v));
        
        //For instance, you could decide to classify as �diet� all dishes with 400 calories or fewer, set to �normal� the dishes having between 400 and 700 calories, and set to �fat� the ones with more than 700 calories

        Map<CaloricLevel, List<Dish>> dishesByCaloricLevel =menu.stream().collect(
        													groupingBy(dish -> {
									                        if (dish.getCalories() <= 400) return CaloricLevel.DIET;
									                        else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
									                        else return CaloricLevel.FAT;
									                        } ));
        dishesByCaloricLevel.forEach((k,v)->System.out.println("Dish Type "+k+" Dishes "+v));
        
        //Multilevel Grouping
        Map<Dish.Type,Map<CaloricLevel, List<Dish>>> dishesByMultiCaloricLevel =menu.stream()
        	   .collect(groupingBy(Dish::getType,
							    groupingBy(dish -> {
				                if (dish.getCalories() <= 400) return CaloricLevel.DIET;
				                else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
				                else return CaloricLevel.FAT;
								} )
						        
        			            )	
        			    );
        
        //System.out.println("Test");
        
        dishesByMultiCaloricLevel.forEach((k,v)->System.out.println("Dish Type "+k+" Dishes "+v));

        //GroupBy Count        
        Map<Dish.Type, Long> typesCount = menu.stream().collect(
                						groupingBy(Dish::getType, counting()));

        typesCount.forEach((k,v)->System.out.println("Dish Type "+k+" Dishes Count "+v));

        //GroupBy Max        
        Map<Dish.Type, Optional<Dish>> mostCaloricByType =
					        		menu.stream()
					        	   .collect(groupingBy(Dish::getType,
					        		maxBy(Comparator.comparingInt(Dish::getCalories))
					        		));
        
        mostCaloricByType.forEach((k,v)->System.out.println("Dish Type "+k+" Dishes "+v));
        
       //GroupBy Max Without Optional       

        Map<Dish.Type,Dish> mostCaloricByType1 =
        		menu.stream()
        	   .collect(groupingBy(Dish::getType,Collectors.collectingAndThen(
        		maxBy(Comparator.comparingInt(Dish::getCalories)),Optional::get
        		)));

        mostCaloricByType1.forEach((k,v)->System.out.println("Dish Type "+k+" Dishes "+v));
        
        //You could also reuse the collector created to sum the calories of all the dishes in the menu to obtain a similar result, but this time for each group of Dishes
        
        Map<Dish.Type, Integer> totalCaloriesByType = menu.stream().collect(groupingBy(Dish::getType,summingInt(Dish::getCalories)));
        totalCaloriesByType.forEach((k,v)->System.out.println("Dish Type "+k+" Total Calories "+v));


        //Which CaloricLevels are available in the menu for each type of Dish. You could achieve this result combining a groupingBy and a mapping collector as follows:
        
        Map<Dish.Type, Set<CaloricLevel>> caloricLevelsByType =menu.stream().collect(
        													   groupingBy(Dish::getType,Collectors.mapping(
        		              dish -> { if (dish.getCalories() <= 400) return CaloricLevel.DIET;
        		                        else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
        		                        else return CaloricLevel.FAT; 
        		                      },Collectors.toSet())
        					 ));
        
        caloricLevelsByType.forEach((k,v)->System.out.println("Dish Type "+k+" CaloricLevels "+v));
        
        //Using Hashset
        caloricLevelsByType =menu.stream().collect(
				   groupingBy(Dish::getType,Collectors.mapping(
					dish -> { if (dish.getCalories() <= 400) return CaloricLevel.DIET;
					else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
					else return CaloricLevel.FAT; 
					},Collectors.toCollection(HashSet::new))
					));

        caloricLevelsByType.forEach((k,v)->System.out.println("Dish Type "+k+" CaloricLevels "+v));

        List<Dish> vegetarianDishes =menu.stream().filter(Dish::isVegetarian).collect(Collectors.toList());
        
        //Using PartitioningBy
        
        Map<Boolean,List<Dish>> partitionedMenu=menu.stream().collect(Collectors.partitioningBy(Dish::isVegetarian));
        vegetarianDishes = partitionedMenu.get(true);
        
        partitionedMenu.forEach((k,v)->System.out.println("Vegetarian "+k+" Dishes "+v));
        
        //Using Multilevel PartitioningBy

        Map<Boolean,Map<Dish.Type,List<Dish>>> partitionedMenuTypeWise=menu.stream().collect(Collectors.partitioningBy(Dish::isVegetarian,Collectors.groupingBy(Dish::getType)));
        partitionedMenuTypeWise.forEach((k,v)->System.out.println("Vegetarian Multilevel "+k+" Dishes "+v));
        
       
        Map<Boolean, Dish> mostCaloricPartitionedByVegetarian =menu.stream().collect(
        													   Collectors.partitioningBy(Dish::isVegetarian,
        													   Collectors.collectingAndThen(
        													   maxBy(Comparator.comparingInt(Dish::getCalories)),
        													   Optional::get)
        													   ));
       
        mostCaloricPartitionedByVegetarian.forEach((k,v)->System.out.println("Vegetarian "+k+" Most Caloric Dishes "+v));

        //Multilevel Partitioning
        menu.stream().collect(Collectors.partitioningBy(
        		             Dish::isVegetarian,Collectors.partitioningBy(d -> d.getCalories() > 500)
        		             )).forEach((k,v)->System.out.println("Vegetarian "+k+" Calories Greater Than 500 "+v));
     
       //Partitioning Counter
       menu.stream().collect(Collectors.partitioningBy(Dish::isVegetarian,counting())).forEach((k,v)->System.out.println("Vegetarian "+k+" Count "+v));
   
       int n=50;
       
       Map<Boolean, List<Integer>> prime=IntStream.rangeClosed(2, n).boxed().collect(Collectors.partitioningBy(candidate -> isPrime(candidate)));
       prime.forEach((k,v)->System.out.println("Prime "+k+" Nos "+v));	
	
	}
	
	public static boolean isPrime(int candidate)
	{
	    int candidateRoot = (int) Math.sqrt((double) candidate);
	    return IntStream.rangeClosed(2, candidateRoot)
	                    .noneMatch(i -> candidate % i == 0);
	}	
}
