package com.sopra.streamoperations;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.sopra.initializer.Initializer;
import com.sopra.models.Dish;

public class MapFlatmapOperations {
	
    static List<Dish> menu;

	
	public static void main(String[] args) {
		
		menu=new Initializer().getMenu();
		
		//Streams support the method map, which takes a function as argument.
		
		List<String> dishNames = menu.stream()
                				.map(d->d.getName())
                				.collect(Collectors.toList());
		
		dishNames.forEach(System.out::println);	
		
		List<String> words = Arrays.asList("Java8", "Lambdas", "In", "Action");
		
		List<Integer> wordLengths = words.stream()
		                                 .map(s->s.length())
		                                 .collect(Collectors.toList());
		
		wordLengths.forEach(System.out::println);	
		
		
		List<Integer> dishNameLengths = menu.stream()
                							.map(Dish::getName)
                							.map(String::length)
                							.collect(Collectors.toList());
		
		dishNameLengths.forEach(System.out::println);	

        //Flattening streams using flatmap method
		//Given the list of words ["Hello", "World"] you�d like to return the list ["H", "e", "l", "o", "W", "r", "d"].

		
		List<String> uniqueCharacters=words.stream()
						                   .map(w->w.split(""))
						                   .flatMap(Arrays::stream)
						                   .distinct()
						                   .collect(Collectors.toList());
	  
	    uniqueCharacters.forEach(System.out::println);	
	
		// Given a list of numbers, how would you return a list of the square of each number? For example, given [1, 2, 3, 4, 5] you should return [1, 4, 9, 16, 25].
		
	    List<Integer> numbers=Arrays.asList(1,2,3,4,5);
	    
	    numbers.stream().map(n->n*n).forEach(System.out::println);
	    
	    //Given two lists of numbers, how would you return all pairs of numbers? For example, given a list [1, 2, 3] and a list [3, 4] you should return [(1, 3), (1, 4), (2, 3), (2, 4), (3, 3), (3, 4)]. For simplicity, you can represent a pair as an array with two elements.

	    List<Integer> numbers1 = Arrays.asList(1, 2, 3);
	    List<Integer> numbers2 = Arrays.asList(3, 4);
	    
	    List<int[]> pairs =numbers1.stream()
	    				 		   .flatMap(i -> numbers2.stream().map(j -> new int[]{i, j}))
	    				 		   .collect(Collectors.toList());
	}	

}
