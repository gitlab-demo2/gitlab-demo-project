package com.sopra.optionaloperations;

import java.util.Optional;

import com.sopra.models.Car;
import com.sopra.models.Insurance;
import com.sopra.models.Person;

public class OptionalOperations {

	public static void main(String args[])
	{
			//We can get hold of an empty optional object using the static factory method Optional.empty:
			Optional<Car> optCar = Optional.empty();

			//We can also create an optional from a non-null value with the static factory method Optional.of:
			//If car were null, a NullPointerException would be immediately thrown (rather than getting a latent error once you try to access properties of the car)
		
			Car car=new Car();
			Insurance insurance=new Insurance();
			insurance.setName("HDFC");
			Person person=new Person();
			
			Optional<Car> optCar1 = Optional.of(car);
			Optional<Insurance> optInsurance = Optional.of(insurance);
			Optional<Person> optPerson = Optional.of(person);
			
			//Finally, by using the static factory method Optional.ofNullable, you can create an Optional object that may hold a null value:
			//If car were null, the resulting Optional object would be empty.
	   
			Optional<Car> optNullableCar = Optional.ofNullable(car);
		
			//	Optional supports a map method for extracting values
			
			Optional<Insurance> optInsurance1 = Optional.ofNullable(insurance);
		
			Optional<String> name = optInsurance1.map(Insurance::getName);
			name = optInsurance1.map(i->i.getName());
            name = optInsurance1.map(i->"ABC");

			
			System.out.println(name);
			
			/*
			 Optional<Person> optPerson1 = Optional.of(person);
			 Optional<String> name1 =optPerson.map(Person::getCar)
			             .map(Car::getInsurance)
			             .map(Insurance::getName);
			
			
		    This code doesn�t compile. Why? The variable optPeople is of type Optional<People>, so it�s perfectly fine to call the map method. But getCar returns an object of type Optional<Car> (as presented in listing 10.4). This means the result of the map operation is an object of type Optional<Optional<Car>>. As a result, the call to getInsurance is invalid because the outermost optional contains as its value another optional, which of course doesn�t support the getInsurance method. Figure 10.3 illustrates the nested optional structure you�d get.
		   
		   */
			
		  Optional<String> name1 =optPerson.flatMap(Person::getCar)
		             					   .flatMap(Car::getInsurance)
		             					   .map(Insurance::getName);
		  
		  System.out.println(name1);

		  
		  String name2 =optPerson.flatMap(Person::getCar)
							     .flatMap(Car::getInsurance)
							     .map(Insurance::getName)
							     .orElse("No Data");
		  
		  System.out.println(name2);

		  //Filtering an optional
		  optInsurance.filter(insur ->"CambridgeInsurance".equals(insurance.getName()))
		              .ifPresent(x -> System.out.println("ok"));
		  
		  /*
		  Supposing the Person class of our Person/Car/Insurance model also has a method getAge to access the age of the person, modify the getCarInsuranceName method in listing 10.5 using the following signature
		  so that the insurance company name is returned only if the person has an age greater than or equal to the minAge argument.
		  */
		  
		  String opt= optPerson.filter(p -> p.getAge() >= 5)
						       .flatMap(Person::getCar)
						       .flatMap(Car::getInsurance)
						       .map(Insurance::getName)
						       .orElse("Unknown");
		   
		  System.out.println(opt);
		  
		  System.out.println(name2);

		  
		  //You can use this method every time you want to safely transform a value that could be potentially null into an optional.
		  //Optional<Object> value = Optional.ofNullable(map.get("key"));

		             				
	}
	
	/*
	 * The methods of the Optional class Method
	 * 
	 * Description
	 * 
	 * empty-> Returns an empty Optional instance
	 * 
	 * filter-> If the value is present
	 * and matches the given predicate, returns this Optional; otherwise returns the
	 * empty one.
	 * 
	 * flatMap->If a value is present, returns the Optional resulting from
	 * the application of the provided mapping function to it; otherwise returns the
	 * empty Optional. 
	 * 
	 * get-> Returns the value wrapped by this Optional if present;
	 * otherwise throws a NoSuchElementException.
	 * 
	 * ifPresent-> If a value is present,
	 * invokes the specified consumer with the value; otherwise does nothing.
	 * 
	 * isPresent-> Returns true if there is a value present; otherwise false 
	 * 
	 * map->If a value is present, applies the provided mapping function to it. 
	 * 
	 * of-> Returns an Optional wrapping the given value or throws a NullPointerException
	 * if this value is null. 
	 * 
	 * ofNullable-> Returns an Optional wrapping the given
	 * value or the empty Optional if this value is null.
	 * 
	 * orElse-> Returns the value
	 * if present or the given default value otherwise.
	 * 
	 * orElseGet-> Returns the value
	 * if present or the one provided by the given Supplier otherwise.
	 * 
	 * orElseThrow->Returns the value if present or throws the exception created by the given
	 * Supplier otherwise.
	 */
}
